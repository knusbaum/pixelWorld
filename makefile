include makefile.inc
LINKS = -lSDL -lSDL_ttf -lGL -lGLU
EXECUTABLE = game
OBJECTS=obj/main.o\
	obj/collisionDetector.o\
	obj/color.o\
	obj/fileFuncs.o\
	obj/font.o\
	obj/fontChar.o\
	obj/fontString.o\
	obj/game.o\
	obj/homeScene.o\
	obj/keyboard.o\
	obj/mainMenu.o\
	obj/gameScene.o\
	obj/rectangle.o\
	obj/sprite.o\
	obj/characterSprite.o

ALL :	objects bin/$(EXECUTABLE)

objects : 
	@$(MAKE) -C src/fonts/
	@$(MAKE) -C src/game/
	@$(MAKE) -C src/gameobjs/
	@$(MAKE) -C src/scenes/

#optimize : CFLAGS = -c -O2 -march=core2 -mtune=core2 -mmmx -msse -msse2 -mssse3 -fomit-frame-pointer -ftree-vectorize -ftree-vectorizer-verbose=5 -o
#optimize : ALL

clean :
	-@rm obj/* bin/* $(EXECUTABLE)
	-@find . -name '*~' -exec rm {} \;
	-@find . -name '*.d' -exec rm {} \;
dir : 
	-@mkdir bin
	-@mkdir obj

bin/$(EXECUTABLE) : $(OBJECTS)
	@echo -e "\t[LD]" $(OBJECTS)
	@$(CXX) $(LINKS) $(OBJECTS) -o bin/$(EXECUTABLE)
	-@ln -s bin/$(EXECUTABLE) $(EXECUTABLE)

-include src/*.d

obj/%.o : src/%.cpp
	@echo -e "\t[CXX]" $*.cpp
	@$(CXX) $(CFLAGS) -o obj/$*.o src/$*.cpp
	@$(CXX) $(CFLAGS) -MM -MT obj/$*.o src/$*.cpp > src/$*.d
