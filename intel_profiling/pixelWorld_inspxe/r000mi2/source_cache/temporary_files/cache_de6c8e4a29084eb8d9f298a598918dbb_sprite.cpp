#include "../includes/sprite.h"

Sprite::Sprite(int width, int height)
    :Rectangle(width, height)
{
}

Sprite::~Sprite()
{
    glDeleteTextures(1, &texture);
}

void Sprite::SetTexture(std::string filename)
{
    glDeleteTextures(1, &texture);
    FileFuncs::LoadGLTexture(texture, filename, GL_LINEAR, GL_LINEAR);
}


void Sprite::Render(void * param, Game * game) const
{   
    glBindTexture(GL_TEXTURE_2D, texture);
    glColor3f(1,1,1);

    glPushMatrix();
    glTranslatef(x,y,0);
    glRotatef(rot, 0, 0, 1);
    glTranslatef(centerx, centery, 0);

    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex3f(0, 0, depth);
    glTexCoord2f(1.0f, 0.0f); glVertex3f(width, 0, depth);
    glTexCoord2f(1.0f, 1.0f); glVertex3f(width, height, depth);
    glTexCoord2f(0.0f, 1.0f); glVertex3f(0, height, depth);
    glEnd();

    glPopMatrix();
}
