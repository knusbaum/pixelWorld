#include "../includes/homeScene.h"

HomeScene::HomeScene()
    :background(640,480), font("res/default_font.bmp"), welcome("Welcome!",100,100, &font)
{
    background.SetTexture("res/floor.bmp");    
}

HomeScene::~HomeScene()
{}

void HomeScene::Update(void * param, Game * game)
{
    keyboard.UpdateState();
    if(keyboard.IsKeyDown(SDLK_RETURN))
    {
        keyboard = Keyboard();
        MainMenu * menu = new MainMenu();
        menu->AddMenuItem("Start");
        menu->AddMenuItem("Quit");
        menu->AddMenuItem("Option 3");
        game->PushScene(menu);
    }
    
}

void HomeScene::Render(void * param, Game * game) const
{
    glTranslatef(x,y,0);
    background.Render(param,game);
    welcome.Render(param,game);
}

void HomeScene::SceneStackCallback(Scene * data, Game * game)
{
    if(data == NULL)
        return;
    
    int selected = ((MainMenu *)data)->GetCurrentItem();
    if(selected == 0)
    {
        glColor4f(1.0f,0.0f,0.0f,1.0f);
        //Start The Game
    }
    else if(selected == 1)
    {
        game->Kill();
    }
    
}
