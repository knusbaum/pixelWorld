#include "../includes/keyboard.h"


Keyboard::Keyboard()
{
    for(int i = 0; i < KEYBOARD_SIZE; i++)
    {
        keystates[i] = false;
    }
}

void Keyboard::UpdateState()
{
    pressedKeys.clear();
    SDL_Event keyevent;
    while(SDL_PollEvent(&keyevent))
    {
        switch(keyevent.type)
        {
        case SDL_KEYDOWN:
            pressedKeys.push_back(keyevent.key.keysym.sym);
            keystates[keyevent.key.keysym.sym] = true;
            break;
        case SDL_KEYUP:
            keystates[keyevent.key.keysym.sym] = false;
            break;
        }
    }

}

bool Keyboard::IsKeyDown(int sdlKey)
{
    return keystates[sdlKey];
}

bool Keyboard::IsKeyPressed(int sdlKey)
{
    for(int i = 0; i < pressedKeys.size(); i++)
        if ( pressedKeys[i] == sdlKey)
            return true;
    return false;
}

