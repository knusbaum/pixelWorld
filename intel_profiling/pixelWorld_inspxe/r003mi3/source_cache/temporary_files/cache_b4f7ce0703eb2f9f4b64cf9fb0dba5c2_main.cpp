#include <iostream>
#include "includes/game.h"
#include "includes/myScene.h"
#include "includes/homeScene.h"

int main()
{
    Game theGame = Game(640, 480);
    //theGame.SetFullscreen(true);
    Color clear = Color(.0f, .0f, 255.0f, 0.01f);

    if(!theGame.Init(&clear))
        return -1;


//    MyScene *scene = new MyScene();

    HomeScene *scene = new HomeScene();
    theGame.SetScene(scene);
    theGame.Run(NULL, NULL);
    

    return 0;
}
