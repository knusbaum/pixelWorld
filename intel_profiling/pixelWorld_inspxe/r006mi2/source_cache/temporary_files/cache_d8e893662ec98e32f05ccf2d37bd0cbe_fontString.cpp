#include "../includes/fontString.h"


FontString::FontString(string str, float x, float y, Font *font)
    :Rectangle(0,0)
{
    this->x = x;
    this->y = y;
    this->font = font;
    SetString(str);
}

void FontString::Render(void * param, Game * game) const
{
    glPushMatrix();
    glTranslatef(x, y, 0);
    glRotatef(rot, 0, 0, 1);
    glTranslatef(centerx, centery, 0);
    glPushMatrix();
    for(int i = 0; i < theString.length(); i++)
    {
        if (theString[i] == '\n')
        {
            glPopMatrix();
            glTranslatef(0, font->GetChar(theString[i])->Height(), 0);
            glPushMatrix();
            continue;
        }
        
        characters[i]->Render(param, game);
        glTranslatef(characters[i]->Width(), 0, 0);
    }
    glPopMatrix();
    glPopMatrix();
}

/*
void FontString::SetPosition(float x, float y)
{
    this->x = x;
    this->y = y;
}

void FontString::SetDepth(float depth)
{
    for(int i = 0; i < characters.size(); i++)
    {
        if(characters[i] != NULL)
            characters[i]->SetDepth(depth);
    }
}

void FontString::Move(float x, float y)
{
    this->x += x;
    this->y += y;
}

void FontString::RotateTo(float angle)
{
    rot = angle;
}

void FontString::RotateBy(float angle)
{
    rot += angle;
}
*/
void FontString::SetString(string str)
{
    characters.clear();
    float newWidth = 0;
    float currentMaxWidth = 0;
    float newHeight = font->GetChar(theString[0])->Height();

    theString = str;
    for(int i = 0; i < theString.length(); i++)
    {
        if (theString[i] == '\n')
        {
            characters.push_back(NULL);
            newHeight+=font->GetChar(theString[i])->Height();
            if(currentMaxWidth > newWidth)
                newWidth = currentMaxWidth;
            currentMaxWidth = 0;
            continue;
        }
        characters.push_back(font->GetChar(theString[i]));
        currentMaxWidth += font->GetChar(theString[i])->Width();
    }

    if(currentMaxWidth > newWidth)
        newWidth = currentMaxWidth;

    SetWidth(newWidth);
    SetHeight(newHeight);
    
}

