#include "../includes/mainMenu.h"

#define MOD_WITH_NEGATIVES(x,m) (x%m + m)%m;

MainMenu::MainMenu()
    :menuBackdrop(300,200)
{
    menuBackdrop.SetTexture("res/main-menu.bmp");
    menuBackdrop.SetCenter(150,100);
    menuBackdrop.SetPosition(320, 240);
}


MainMenu::~MainMenu()
{}

void MainMenu::Update(void * param, Game *game)
{
    keyboard.UpdateState();

    if(keyboard.IsKeyDown(SDLK_RETURN))
        game->PopScene();

    if(keyboard.IsKeyPressed(SDLK_DOWN))
        currentItem = (currentItem + 1) % menuItems.size();

    if(keyboard.IsKeyPressed(SDLK_UP))
        currentItem = MOD_WITH_NEGATIVES(currentItem - 1, menuItems.size());
}

void MainMenu::Render(void * param, Game * game) const
{
    glTranslatef(x,y,0);
    
    menuBackdrop.Render(param,game);
    glTranslatef(menuBackdrop.GetLeft(), menuBackdrop.GetTop(), 0);
    
    for(int i = 0; i < menuItems.size(); i++)
    {
        if(i == currentItem)
            glColor4f(1.0f,1.0f,1.0f,1.0f);
        else
            glColor4f(0.5f,1.0f,1.0f,1.0f);
            
        menuItems[i]->Render(param, game);
    }
}

void MainMenu::SceneStackCallback(Scene *, Game *) {} ;

int MainMenu::GetCurrentItem()
{
    return currentItem;
}

void MainMenu::AddMenuItem(string s)
{
    int currentY = 30;
    if(menuItems.size() > 0)
        currentY = menuItems[menuItems.size() - 1]->GetY() + 15;
 
    menuItems.push_back(new FontString(s, 15, currentY, &font));
}

