#include <iostream>
#include "includes/game.h"
#include "includes/homeScene.h"

int main()
{
    Game theGame = Game(640, 360);
//    theGame.SetFullscreen(true);
    Color clear = Color(.0f, .0f, .0f, 1.0f);

    if(!theGame.Init(&clear))
        return -1;


//    MyScene *scene = new MyScene();

    HomeScene *scene = new HomeScene();
    theGame.SetScene(scene);
    theGame.Run(NULL, NULL);
    

    return 0;
}
