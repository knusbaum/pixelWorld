#include "../includes/color.h"

Color::Color()
{
    red = 0;
    blue = 0;
    green = 0;
    alpha = 0;
}

Color::Color(float red, float green, float blue, float alpha)
{
    this->red = red;
    this->green = green;
    this->blue = blue;
    this->alpha = alpha;
}

Color::~Color(){}

float Color::Red()
{
    return red;
}

float Color::Green()
{
    return green;
}

float Color::Blue()
{
    return blue;
}

float Color::Alpha()
{
    return alpha;
}
