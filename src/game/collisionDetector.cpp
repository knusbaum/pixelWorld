#include "../includes/collisionDetector.h"

CollisionDetector::CollisionDetector(){}

void CollisionDetector::AddRectangle(Rectangle * rect)
{
    rectangles.push_back(rect);
    rectangles.sort(rect_compare);
}

void CollisionDetector::RemoveRectangle(Rectangle * rect)
{
    rectangles.remove(rect);
}

list<Collision> * CollisionDetector::GetCollisions()
{
    rectangles.sort(rect_compare);
    collisions.clear();
    list<Rectangle *>::iterator it = rectangles.begin();
    list<Rectangle *>::iterator activeIt;

    list<Rectangle *> activeList;
    
    activeList.push_back(*it);

    float currentRight = (*it)->GetX() + (*it)->GetWidth();
    
    for(it++; it != rectangles.end(); it++)
    {

        for(activeIt = activeList.begin(); 
            activeIt != activeList.end();)
        {
//            float currentLeftBound = (*it)->GetX() + (*it)->GetCenterX();
//            float activeRightBound = (*activeIt)->GetX() + (*activeIt)->GetWidth() + (*activeIt)->GetCenterX();
            
            //printf("Active Rectangle: Lft:%f, Rgt:%f, Top:%f, Bot:%f, Rot:%f, X:%f, Y:%f\n", (*activeIt)->GetLeft(), (*activeIt)->GetRight(), (*activeIt)->GetTop(), (*activeIt)->GetBottom(), (*activeIt)->GetRotation(), (*activeIt)->GetX(), (*activeIt)->GetY());
            if((*it)->GetLeft() <= (*activeIt)->GetRight())
            {
                if(withinYBounds(*it, *activeIt))
                    collisions.push_back(Collision((*activeIt), (*it)));
                activeIt++;
            }
            else
            {
                activeList.erase(activeIt++);
            }
        }
        activeList.push_back(*it);
    }

    return &collisions;
}

bool CollisionDetector::withinYBounds(Rectangle * rect1, Rectangle * rect2)
{
    if(rect1->GetY() < rect2->GetY())
    {
        if(rect2->GetTop() <= rect1->GetBottom())
            return true;
    }
    else
    {
        if(rect1->GetTop() < rect2->GetBottom())
            return true;
    }
    
    return false;
}

bool rect_compare(Rectangle *rect1, Rectangle *rect2)
{
    return rect1->GetX() < rect2->GetX();
}
