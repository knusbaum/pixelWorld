#include "../includes/fileFuncs.h"

SDL_Surface* FileFuncs::LoadBMP(const char *Filename)
{
  if(!Filename)
    return NULL;
  
  FILE *bitmap;
  bitmap = fopen(Filename, "r");
  if(bitmap)
    {
      fclose(bitmap);
      return SDL_LoadBMP(Filename);
    }
  return NULL;
}

bool FileFuncs::LoadGLTexture(GLuint &texture, std::string texName, GLint GLTexMin, GLint GLTexMag)
{
    SDL_Surface *bitmap = LoadBMP(texName.c_str());
    if(bitmap)
    {
        FileFuncs::LoadGLTexture(texture, bitmap, GLTexMin, GLTexMag);
        SDL_FreeSurface(bitmap);
        return true;
    }
    return false;
}

void FileFuncs::LoadGLTexture(GLuint &texture, SDL_Surface* bitmap, GLint GLTexMin, GLint GLTexMag)
{

    int colors = bitmap->format->BytesPerPixel;
    int texture_format;
    if(colors == 4)
    {
        if(bitmap->format->Rmask == 0x000000ff)
            texture_format = GL_RGBA;
        else
            texture_format = GL_BGRA;
    }
    else
    {
        if(bitmap->format->Rmask == 0x000000ff)
            texture_format = GL_RGB;
        else
            texture_format = GL_BGR;
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
     glTexImage2D(GL_TEXTURE_2D, 0, colors, bitmap->w, bitmap->h, 0, texture_format, GL_UNSIGNED_BYTE, bitmap->pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GLTexMin);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GLTexMag);
    
}
