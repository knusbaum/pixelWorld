#include "../includes/game.h"

using std::cout;
using std::cerr;
using std::endl;

Game::Game(int width, int height)
{
    this->width = width;
    this->height = height;
    Surf_Display = NULL;
    fullscreen = false;
}

Game::~Game()
{
    for(int i = 0; i < scenes.size(); i++)
        delete scenes[i];
    
    scenes.clear();
    
    SDL_FreeSurface(Surf_Display);
    SDL_Quit();
    //if(currentScene != NULL)
    //    delete(currentScene);
}

bool Game::Init(Color *clearColor)
{
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
        return false;

    //Basic Attributes of an openGL rendering context
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,             8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,           8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,            8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,           8);
 
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,          16);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,         32);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,         1);

    SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE,       8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE,     8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE,      8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE,     8);

    const SDL_VideoInfo *videoInfo;
    videoInfo = SDL_GetVideoInfo( );
    
    if ( !videoInfo )
    {
        cerr<<"Video query failed: "<<SDL_GetError();
        return false;
    }
    
    int videoFlags;
    videoFlags  = SDL_OPENGL;
    videoFlags |= SDL_GL_DOUBLEBUFFER;
    videoFlags |= SDL_HWPALETTE;
    videoFlags |= SDL_RESIZABLE;
    if(fullscreen)
        videoFlags |= SDL_FULLSCREEN;
    
    if ( videoInfo->hw_available )
        videoFlags |= SDL_HWSURFACE;
    else
        videoFlags |= SDL_SWSURFACE;

    /* This checks if hardware blits can be done */
    if ( videoInfo->blit_hw )
        videoFlags |= SDL_HWACCEL;

    Surf_Display = SDL_SetVideoMode(width, height, videoInfo->vfmt->BitsPerPixel, videoFlags);
    if(Surf_Display == NULL)
    {
        cout<< "Failed to set video mode: "<<SDL_GetError()<<endl;
        return false;
    }

    /* True Type Font Lib Setup */
    if(TTF_Init() == -1)
    {
        cout<<"Failed to initialize SDL_ttf: "<<SDL_GetError()<<endl;
        return false;
    }

    /* OpenGL Setup */

    glClearColor(clearColor->Red(), clearColor->Green(), clearColor->Blue(), clearColor->Alpha());
    glClearDepth(1.0f);
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_BLEND); //Enable alpha blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set the blend function

    glViewport(0, 0, width, height);
    
    //Set up "camera" for 2D (orthological) rendering
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, height, 0, -1, 1);
    
    //Switch to model view for drawing.
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_TEXTURE_2D);
    glLoadIdentity();

    return true;
}

void Game::Kill()
{
    running = false;
}

void Game::SetScene(Scene *scene)
{
    if(scene != NULL)
    {
        if(scenes.size() > 0)
        {
            delete scenes[scenes.size()-1];
            scenes[scenes.size()-1] = scene;
        }
        else
            scenes.push_back(scene);
    }
}

void Game::PushScene(Scene *scene)
{
    if(scene != NULL)
        scenes.push_back(scene);
}

void Game::PopScene()
{
    Scene * popped = scenes.back();
    scenes.pop_back();
    if(scenes.size() > 0)
        scenes[0]->SceneStackCallback(popped, this);
    delete popped;
}

void Game::SetFullscreen(bool fullscreen)
{
    this->fullscreen = fullscreen;
}

SDL_Surface *Game::getSurfDisplay()
{
    return Surf_Display;
}


void Game::Run(void* updateParam, void* renderParam)
{
    running = true;
    if(!scenes.empty())
        while(running)
        {
            update(updateParam);
            render(renderParam);
        }
}

/* member functions */
void Game::update(void * updateParam)
{
    scenes.back()->Update(updateParam, this);
}

void Game::render(void * renderParam)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    for(int i = 0; i < scenes.size(); i++)
        scenes[i]->Render(renderParam, this);

    SDL_GL_SwapBuffers();
}


