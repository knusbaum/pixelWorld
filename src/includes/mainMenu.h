#ifndef MAINMENU_H
#define MAINMENU_H

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <sstream>
#include "menu.h"
#include "scene.h"
#include "game.h"
#include "sprite.h"
#include "keyboard.h"

using std::stringstream;
using std::string;

class MainMenu : public Menu
{
    Sprite menuBackdrop;
    
public:
    MainMenu();
    virtual ~MainMenu();
    virtual void Update(void *, Game *);
    virtual void Render(void *, Game *) const;
    virtual void SceneStackCallback(Scene *, Game *);
    virtual int GetCurrentItem();
    virtual void AddMenuItem(string s);
};

#endif
