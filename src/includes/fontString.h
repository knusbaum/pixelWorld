#ifndef FONTSTRING_H
#define FONTSTRING_H
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <vector>
#include "rectangle.h"
#include "game.h"
#include "font.h"
#include "fontChar.h"

using std::string;
using std::vector;

class FontString : public Rectangle{

    string theString;
    //float x, y, rot;
    Font *font;
    vector<FontChar*> characters;

public:
    FontString(string str, float x, float y, Font *font);
    
    //void SetPosition(float x, float y);
    //void SetDepth(float depth);
    //void Move(float x, float y);
    //void RotateTo(float angle);
    //void RotateBy(float angle);
    void SetString(string str);
    void Render(void * param, Game * game) const;
};

#endif
