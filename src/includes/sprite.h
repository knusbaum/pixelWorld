#ifndef SPRITE_H
#define SPRITE_H

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <string>
#include "rectangle.h"
#include "fileFuncs.h"
#include "game.h"

using std::string;

class Sprite : public Rectangle{
    GLuint texture;
    int swidth, sheight;
    float left, right, top, bottom;
    bool reverse;

protected:    
    int currentSprite;
    void SetCurrentSpriteDimensions();

public:
    Sprite(int width, int height);
    Sprite(int width, int height, int spriteWidth, int spriteHeight);
    virtual ~Sprite();
    void SetTexture(string filename);
    virtual void Render(void * param, Game * game) const;
    void SetIndividualDimensions(int width, int height);
    void SetCurrentSprite(int sprite);
    int GetCurrentSprite();
    void SetReverse(bool val);
};
#endif
