#ifndef CHARACTERSPRITE_H
#define CHARACTERSPRITE_H

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <string>
#include "rectangle.h"
#include "fileFuncs.h"
#include "game.h"
#include "sprite.h"

using std::string;

class CharacterSprite : public Sprite{
    int directionCount;
    int currentDirection;
    int starts[4];
    int sheetDirection;
    int spriteSteps;
    int currentStep;

public:
    CharacterSprite(int width, int height, int spriteWidth, int spriteHeight, int directioncount);
    virtual ~CharacterSprite();
    
    void SetStarts(int up, int down, int left, int right);
    void SetDirectionCount(int directionCount);
    void SetCurrentDirection(int direction);
    void SetAnimation(int animation);
    void NextAnimation();
    void StepAnimation();
    void SetSpriteStep(int step);

};

#endif
