#ifndef MENU_H
#define MENU_H
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <list>
#include <string>
#include "scene.h"
#include "game.h"
#include "font.h"
#include "fontString.h"

using std::vector;
using std::string;

class Menu : public Scene{

protected:
    Keyboard keyboard;
    vector<FontString> menuItems;
    Font font;
    int currentItem;

public:
    Menu() :font("res/default_font.bmp", 10), currentItem(0) {} 
    virtual ~Menu()
    {
//        while(!menuItems.empty())
//        {
//            delete menuItems.back();
//            menuItems.pop_back();
//        }
    }

    virtual void Update(void *, Game *) = 0;
    virtual void Render(void *, Game *) const = 0;
    //virtual void SceneStackCallback(void *, Game *) = 0;
    virtual int GetCurrentItem() = 0;
    virtual void AddMenuItem(string s) = 0;
};

#endif
