#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <cmath>
#include "renderable.h"
#include "game.h"

#define degtorad( x ) ( x*0.0174532925 )

typedef struct
{
    float x, y;
} point_t;

class Rectangle : Renderable{

protected:
    float x;
    float y;
    float rot;
    int width;
    int height;
    float depth;
    float centerx;
    float centery;

public:
    Rectangle(int width, int height);
    virtual ~Rectangle();

    void SetPosition(float x, float y);
    void SetCenter(float x, float y);
    void SetDepth(float x);
    void SetWidth(float w);
    void SetHeight(float h);
    void Move(float x, float y);
    void RotateTo(float angle);
    void RotateBy(float angle);

    float GetX() const;
    float GetY() const;
    float GetWidth() const;
    float GetHeight() const;
    float GetCenterX() const;
    float GetCenterY() const;

    float GetLeft() const;
    float GetRight() const;
    float GetTop() const;
    float GetBottom() const;
    float GetRotation() const;

    virtual void Render(void * param, Game * game) const = 0;

};

#endif
