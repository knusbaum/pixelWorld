#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <sstream>
#include "scene.h"
#include "game.h"
#include "sprite.h"
#include "font.h"
#include "fontString.h"
#include "mainMenu.h"
#include "homeScene.h"
#include "characterSprite.h"

class GameScene : public Scene
{

    Sprite currentBackground;
    CharacterSprite character;

public:
    GameScene();
    virtual ~GameScene();
    void Update(void *, Game *);
    void Render(void *, Game *) const;
    void SceneStackCallback(Scene *, Game *);
};

#endif
