#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>

namespace FileFuncs{

    SDL_Surface* LoadBMP(const char *Filename);
    bool LoadGLTexture(GLuint &texture, std::string texName, GLint GLTexMin, GLint GLTexMag);
    void LoadGLTexture(GLuint &texture, SDL_Surface* bitmap, GLint GLTexMin, GLint GLTexMag);

}
