#ifndef RENDERABLE_H
#define RENDERABLE_H

class Game;

class Renderable
{

public:
    Renderable(){};
    virtual void Render(void *, Game *) const = 0;
};
#endif
