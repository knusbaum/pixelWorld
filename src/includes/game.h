#ifndef GAME_H
#define GAME_H

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <vector>
#include "color.h"
#include "scene.h"
#include "keyboard.h"

using std::vector;

class Game{
    
    int width, height;
    bool running;
    bool fullscreen;
    SDL_Surface *Surf_Display;
    vector<Scene *>scenes;

    void update(void*);
    void render(void*);
    

public:
    Keyboard keyboard;

    Game(int width, int height);
    ~Game();

    bool Init(Color *clearColor);
    void Kill();

    void SetScene(Scene *scene);
    void PushScene(Scene *scene);
    void PopScene();
    void SetFullscreen(bool fullscreen);
    SDL_Surface* getSurfDisplay();
    
    void Run(void* updateParam, void* renderParam);
};
#endif
