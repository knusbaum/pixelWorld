#ifndef SCENE_H
#define SCENE_H

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <list>
#include "renderable.h"

using std::list;

class Game;

class Scene : public Renderable
{

protected:
    float x, y;
    list<Renderable*> renderables;
    
public:
    Scene() :x(0), y(0) {};
    virtual ~Scene(){};
    virtual void Update(void *, Game *) = 0;
    virtual void Render(void *, Game *) const = 0;
    virtual void SceneStackCallback(Scene *, Game *) = 0;
//    virtual void Copy(Scene **) = 0; 

};
#endif
