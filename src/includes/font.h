#ifndef FONT_H
#define FONT_H
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include "fileFuncs.h"
#include "fontChar.h"

using std::string;

class Font{
    GLuint font;
    int width, height;
    FontChar *characters[48];
    //Alphabet a-z is index 0-25
    //Numbers are index 26-35
    //Space = 36
    //" = 37
    //! = 38
    //. = 39
    //, = 40
    //? = 41
    //; = 42
    /// = 43
    //\ = 44
    //# = 45
    //$ = 46
    //UNKNOWN = 47
    void freeCharacters();
    void setupCharacters();
    void loadFont(string fontFile);
    

public:
    Font(string fontFile, int height = 20);
    ~Font();
    
    FontChar *GetChar(const char);
    void SetHeight(int height);
    

};

#endif
