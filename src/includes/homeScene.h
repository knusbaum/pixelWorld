#ifndef HOMESCENE_H
#define HOMESCENE_H

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <sstream>
#include "scene.h"
#include "game.h"
#include "sprite.h"
#include "font.h"
#include "fontString.h"
#include "keyboard.h"
#include "mainMenu.h"
#include "gameScene.h"

using std::stringstream;

class HomeScene : public Scene
{
    Sprite background;
    Font font;
    FontString welcome;
    
    void mainMenuCallback(void * data, Game * game);

public:
    HomeScene();
    virtual ~HomeScene();
    void Update(void *, Game *);
    void Render(void *, Game *) const;
    void SceneStackCallback(Scene *, Game *);
};

#endif
