#ifndef FONTCHAR_H
#define FONTCHAR_H
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "rectangle.h"

class FontChar : Renderable{

    int char_w, char_h;
    float xbegin, xend, ybegin, yend, depth;
    GLuint *font;
public:

    FontChar(GLuint *font, int tex_w, int tex_h,  int char_x, int char_y, int char_w, int char_h, float depth);
    
    void Render(void * param, Game * game) const;
    void SetDepth(float depth);
    int Width();
    int Height();

    void SetWidth(int width);
    void SetHeight(int height);
};


#endif
