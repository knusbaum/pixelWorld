#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H

#include <list>
#include <queue>
#include <stdio.h>
#include "rectangle.h"

using std::list;
using std::queue;

class Collision{
public:    
    Collision(Rectangle * rect1, Rectangle * rect2)
    {this->rect1 = rect1; this->rect2 = rect2; };
    Rectangle * GetRect1(){ return rect1; };
    Rectangle * GetRect2(){ return rect2; };
    
private:
    Rectangle * rect1;
    Rectangle * rect2;
};

class CollisionDetector{

public:
    CollisionDetector();
    
    void AddRectangle(Rectangle * rect);
    void RemoveRectangle(Rectangle * rect);

    list<Collision> * GetCollisions();

private:    
    
    list<Collision> collisions;

    list<Rectangle * > rectangles;

    //bool rect_compare(Rectangle * rect1, Rectangle * rect2);

    bool withinYBounds(Rectangle * rect1, Rectangle * rect2);

};

bool rect_compare(Rectangle * rect1, Rectangle * rect2);

#endif
