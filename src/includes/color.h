#ifndef COLOR_H
#define COLOR_H

class Color{
    float red;
    float green;
    float blue;
    float alpha;
public:
    Color();
    Color(float red, float green, float blue, float alpha);
    ~Color();
    float Red();
    float Green();
    float Blue();
    float Alpha();
};

#endif
