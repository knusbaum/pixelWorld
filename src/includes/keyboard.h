#ifndef KEYBOARD_H
#define KEYBOARD_H
#include <SDL/SDL.h>
#include <vector>

#define KEYBOARD_SIZE 323

using std::vector;

class Keyboard{
    bool keystates[KEYBOARD_SIZE];
    vector<int> pressedKeys;
public:
    Keyboard();

    void UpdateState();
    bool IsKeyDown(int sdlKey);
    bool IsKeyPressed(int sdlKey);
};

#endif
