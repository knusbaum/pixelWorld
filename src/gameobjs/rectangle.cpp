#include "../includes/rectangle.h"

Rectangle::Rectangle(int width, int height)
{
    this->width = width;
    this->height = height;
    centerx = 0;
    centery = 0;
    x = 0;
    y = 0;
    depth = 1.0f;
    rot = 0;
    
}

Rectangle::~Rectangle()
{
}

void Rectangle::SetPosition(float x, float y)
{
    this->x = x;
    this->y = y;
}

void Rectangle::SetCenter(float x, float y)
{
    this->centerx = -x;
    this->centery = -y;
}

void Rectangle::SetDepth(float depth)
{
    this->depth = depth;
}

void Rectangle::SetWidth(float w)
{
    this->width = w;
}

void Rectangle::SetHeight(float h)
{
    this->height = h;
}

void Rectangle::Move(float x, float y)
{
    this->x += x;
    this->y += y;
}

void Rectangle::RotateTo(float angle)
{
    rot = angle;

    while(rot >= 360)
        rot-=360;
    
    while(rot<0)
        rot+=360;
}

void Rectangle::RotateBy(float angle)
{
    rot += angle;
    
    while(rot >= 360)
        rot-=360;
    while(rot<0)
        rot+=360;
    
}

float Rectangle::GetX() const
{
    return x;
}
float Rectangle::GetY() const
{
    return y;
}
float Rectangle::GetWidth() const
{
    return width;
}

float Rectangle::GetHeight() const
{
    return height;
}

float Rectangle::GetCenterX() const
{
    return centerx;
}

float Rectangle::GetCenterY() const
{
    return centery;
}

float Rectangle::GetLeft() const
{

    float theta = degtorad(rot);
    
    float cosine = cos(theta);
    float sine = sin(theta);

    if(rot <= 90)
        return x + (((centerx) * cosine) - ((centery + height) * sine));
    else if(rot <= 180)
        return x + (((centerx + width) * cosine) - ((centery + height) * sine));
    else if(rot <= 270)
        return x + (((centerx + width) * cosine) - (centery * sine));
    else
        return x + ((centerx * cosine) - (centery * sine));
/*
    point_t corners[4];

    corners[0] = {x + centerx, y + centery}; //upperLeft
    corners[1] = {x + centerx + width, y + centery}; //upperRight
    corners[2] = {x + centerx, y + centery + height}; //lowerLeft
    corners[3] = {x + centerx + width, y + centery + height}; //lowerRight
    

//    return x + centerx;
    float theta = degtorad(rot);
    
    float cosine = cos(theta);
    float sine = sin(theta);

    float o_vx = corners[0].x - x;
    float o_vy = corners[0].y - y;
    float o_tx = x + (o_vx * cosine - o_vy * sine);

    float minX = o_tx;

    for(int i = 1; i < 4; i++)
    {
        float vx = corners[i].x - x;
        float vy = corners[i].y - y;
        float tx = x + (vx * cosine - vy * sine);
        if(tx < minX)
            minX = tx;
    }

    //return x + centerx;
    //return ((x + centerx) * cosine) - ((y + centery) * sine);
    return minX;
*/
}

float Rectangle::GetRight() const
{

    float theta = degtorad(rot);
    
    float cosine = cos(theta);
    float sine = sin(theta);

    if(rot <= 90)
        return x + (((centerx + width) * cosine) - (centery * sine));
    else if(rot <= 180)
        return x + ((centerx * cosine) - (centery * sine));
    else if(rot <= 270)
        return x + (((centerx) * cosine) - ((centery + height) * sine));
    else
        return x + (((centerx + width) * cosine) - ((centery + height) * sine));
        
}

float Rectangle::GetTop() const
{

    float theta = degtorad(rot);
        
    float cosine = cos(theta);
    float sine = sin(theta);

    if(rot <= 90)
        return y + ((centerx * sine) + (centery * cosine));
    else if(rot <= 180)
        return y + (((centerx) * sine) + ((centery + height) * cosine));
    else if(rot <= 270)
        return y + (((centerx + width) * sine) + ((centery + height) * cosine)); 
    else
        return y + (((centerx + width) * sine) + (centery * cosine));

    /*
    if(rot <= 90)
        return y + ((centery * cosine) - ((centerx + width) * sine));
    else if(rot <= 180)
        return y + (((centery + height) * cosine) - ((centerx + width) * sine));
    else if(rot <= 270)
        return y + (((centery + height) * cosine) - ((centerx) * sine));
    else
        return y + ((centery * cosine) - (centerx * sine));
    */  
}

float Rectangle::GetBottom() const
{

    float theta = degtorad(rot);
    
    float cosine = cos(theta);
    float sine = sin(theta);

    if(rot <= 90)
        return y + (((centerx + width) * sine) + ((centery + height) * cosine));
    else if(rot <= 180)
        return y + (((centerx + width) * sine) + (centery * cosine));
    else if(rot <= 270)
        return y + ((centerx * sine) + (centery * cosine));
    else
        return y + (((centerx) * sine) + ((centery + height) * cosine));
}

float Rectangle::GetRotation() const
{
    return rot;
}
