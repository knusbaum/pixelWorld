#include "../includes/characterSprite.h"

CharacterSprite::CharacterSprite(int width, int height, 
                                 int spriteWidth, int spriteHeight,
                                 int directioncount)
    :Sprite(width, height, spriteWidth, spriteHeight), 
     directionCount(directioncount), currentDirection(0), sheetDirection(1), 
     spriteSteps(1), currentStep(1)
{
    SetStarts(0,0,0,0);
}

CharacterSprite::~CharacterSprite()
{
}

void CharacterSprite::SetStarts(int up, int down, int left, int right)
{
    starts[0] = up;
    starts[1] = down;
    starts[2] = left;
    starts[3] = right;
}

void CharacterSprite::SetDirectionCount(int count)
{
    directionCount = count;
}

void CharacterSprite::SetCurrentDirection(int direction)
{
    if(currentDirection != direction)
    {
        SetReverse(direction == 3);
        currentDirection = direction;
        SetAnimation(0);
    }
}

void CharacterSprite::SetAnimation(int animation)
{
    currentSprite = (animation % directionCount) + starts[currentDirection];
    SetCurrentSpriteDimensions();
}

void CharacterSprite::NextAnimation()
{
    int curr = (currentSprite - starts[currentDirection] + sheetDirection) 
        % directionCount;
    if(sheetDirection > 0 && curr == 0 || sheetDirection < 0 && curr == -1)
        sheetDirection = -sheetDirection;
    SetAnimation((currentSprite - starts[currentDirection]) + sheetDirection);
}

void CharacterSprite::StepAnimation()
{
    currentStep = (currentStep + 1) % spriteSteps;
    if(currentStep == 0)
        NextAnimation();
}

void CharacterSprite::SetSpriteStep(int step)
{
    spriteSteps = step;
}
