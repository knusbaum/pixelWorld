#include "../includes/sprite.h"

Sprite::Sprite(int width, int height)
    :Rectangle(width, height), swidth(width), sheight(height),
     currentSprite(0), left(0), right(1), top(0), bottom(1), reverse(false)
{
}

Sprite::Sprite(int width, int height, int spriteWidth, int spriteHeight)
    :Rectangle(width, height), swidth(spriteWidth), sheight(spriteHeight),
     currentSprite(0)
{
    SetCurrentSpriteDimensions();
}

Sprite::~Sprite()
{
    glDeleteTextures(1, &texture);
}

void Sprite::SetTexture(std::string filename)
{
    glDeleteTextures(1, &texture);
    FileFuncs::LoadGLTexture(texture, filename, GL_LINEAR, GL_LINEAR);
}


void Sprite::Render(void * param, Game * game) const
{   
    glBindTexture(GL_TEXTURE_2D, texture);
    glColor3f(1,1,1);

    glPushMatrix();
    glTranslatef(x,y,0);
    glRotatef(rot, 0, 0, 1);
    glTranslatef(centerx, centery, 0);

    if(reverse)
    {
        glBegin(GL_QUADS);
        glTexCoord2f(right, top); glVertex3f(0, 0, depth);
        glTexCoord2f(left, top); glVertex3f(swidth, 0, depth);
        glTexCoord2f(left, bottom); glVertex3f(swidth, sheight, depth);
        glTexCoord2f(right, bottom); glVertex3f(0, sheight, depth);
        glEnd();
    }
    else
    {
        glBegin(GL_QUADS);
        glTexCoord2f(left, top); glVertex3f(0, 0, depth);
        glTexCoord2f(right, top); glVertex3f(swidth, 0, depth);
        glTexCoord2f(right, bottom); glVertex3f(swidth, sheight, depth);
        glTexCoord2f(left, bottom); glVertex3f(0, sheight, depth);
        glEnd();
    }
    glPopMatrix();
}

void Sprite::SetIndividualDimensions(int width, int height)
{
    swidth = width;
    sheight = height;
    SetCurrentSpriteDimensions();
}

void Sprite::SetCurrentSprite(int sprite)
{
    currentSprite = sprite;
    SetCurrentSpriteDimensions();
}

int Sprite::GetCurrentSprite()
{
    return currentSprite;
}

void Sprite::SetCurrentSpriteDimensions()
{
    left = (currentSprite * swidth) % width;
    right = left + swidth;
    top = ((currentSprite * swidth) / width) * sheight;
    bottom = top + sheight;

    left /= width;
    right /= width;
    top /= height;
    bottom /= height;

//    if(reverse)
//    {
//        left = left - 1.0f;
//        right = right - 1.0f;
//    }

}

void Sprite::SetReverse(bool val)
{
    reverse = val;
}
