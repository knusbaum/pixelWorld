#include "../includes/font.h"

Font::Font(string fontFile, int height)
{

    loadFont(fontFile);
    setupCharacters();
    SetHeight(height);
}

Font::~Font()
{
    freeCharacters();
}

void Font::freeCharacters()
{
    for(int i = 0; i < 48; i++)
        delete characters[i];
}

void Font::setupCharacters()
{
    int charHeight = 20;
    //Alphabet
    for(int i = 0; i < 26; i++)
    {
        FontChar *c = new FontChar(&font, width, height, (i % 10) * charHeight, (i / 10) * charHeight, charHeight, charHeight, 1.0f);
        characters[i] = c;
    }

    //Numbers
    for(int i = 0; i < 10; i++)
    {
        FontChar *c = new FontChar(&font, width, height, i* charHeight, 3 * charHeight, charHeight, charHeight, 1.0f);
        characters[26+i] = c;
    }

    //Space
    {
        FontChar *c = new FontChar(&font, width, height, 1, 4 * charHeight, charHeight, charHeight, 1.0f);
        characters[36] = c;
    }

    //Small Symbols
    for(int i = 0; i < 8; i++)
    {
        FontChar *c = new FontChar(&font, width, height, charHeight + (i * (charHeight/2)), 4 * charHeight, charHeight/2, charHeight,  1.0f);
        characters[37 + i] = c;
    }

    //# $ and UNKNOWN
    for(int i = 0; i < 3; i++)
    {
        FontChar *c = new FontChar(&font, width, height, 140 + (i * 20), 80, 20, 20, 1.0f);
        characters[45 + i] = c;
    }
}

void Font::SetHeight(int height)
{
    //Alphabet, Numbers, Space
    for(int i = 0; i < 37; i++)
    {
        characters[i]->SetWidth(height);
        characters[i]->SetHeight(height);
    }

    //Small Symbols
    for(int i = 0; i < 8; i++)
    {
        characters[37 + i]->SetWidth(height/2);
        characters[37 + i]->SetHeight(height);
    }

    //# $ and UNKNOWN
    for(int i = 0; i < 3; i++)
    {
        characters[45 + i]->SetWidth(height);
        characters[45 + i]->SetHeight(height);
    } 
}

void Font::loadFont(string fontFile)
{
    SDL_Surface *bitmap = FileFuncs::LoadBMP(fontFile.c_str());
    if(bitmap)
    {
        width = bitmap->w;
        height = bitmap->h;
        FileFuncs::LoadGLTexture(font, bitmap, GL_NEAREST, GL_NEAREST);
        SDL_FreeSurface(bitmap);
    }
}

FontChar *Font::GetChar(const char character)
{
    if(character >= 65 && character <= 90)
        return characters[character-65];
    else if(character >= 97 && character <= 122)
        return characters[character - 97];
    else if(character >= 48 && character <= 57)
        return characters[character - 22];
    else
    {
        switch(character){
        case 32:
            return characters[36];
            break;
        case 34:
            return characters[37];
            break;
        case 33:
            return characters[38];
            break;
        case 46:
            return characters[39];
            break;
        case 44:
            return characters[40];
            break;
        case 63:
            return characters[41];
            break;
        case 59:
            return characters[42];
            break;
        case 47:
            return characters[43];
            break;
        case 92:
            return characters[44];
            break;
        case 35:
            return characters[45];
            break;
        case 36:
            return characters[46];
            break;
        default:
            return characters[47];
            break;
        }
    }
}
