#include "../includes/fontChar.h"

FontChar::FontChar(GLuint *font, int tex_w, int tex_h, int char_x, int char_y, int char_w, int char_h, float depth)
{
    this->font = font;
  
    xbegin = (float)char_x/(float)tex_w;
    ybegin = (float)char_y/(float)tex_h;
    xend = (float)(char_w+char_x)/(float)tex_w;
    yend = (float)(char_h+char_y)/(float)tex_h;

    this->char_w = char_w;
    this->char_h = char_h;
    this->depth = depth;
}

void FontChar::Render(void *param, Game * game) const
{
//    glColor4f(1.0f,1.0f,1.0f, 1.0f);
    glBindTexture(GL_TEXTURE_2D, *font);

    glBegin(GL_QUADS);
    glTexCoord2f(xbegin, ybegin); glVertex3f(0,0, depth);
    glTexCoord2f(xend, ybegin); glVertex3f(char_w, 0, depth);
    glTexCoord2f(xend, yend); glVertex3f(char_w, char_h, depth);
    glTexCoord2f(xbegin, yend); glVertex3f(0, char_h, depth);
    glEnd();
}

int FontChar::Width()
{
    return char_w;
}

int FontChar::Height()
{
    return char_h;
}

void FontChar::SetDepth(float depth)
{
    this->depth = depth;
}

void FontChar::SetWidth(int width)
{
    this->char_w = width;
}

void FontChar::SetHeight(int height)
{
    this->char_h = height;
}
