#include "../includes/gameScene.h"

GameScene::GameScene()
    :currentBackground(640,480), character(128,144,16,24,3)
{
    currentBackground.SetTexture("res/floor.bmp");
    character.SetTexture("res/Spaceman.bmp");
    //vharacter.SetIndividualDimensions(16,24);
    character.SetStarts(3,0,6,6);
    character.SetPosition(320,240);
    character.SetSpriteStep(5);
}

GameScene::~GameScene()
{}

void GameScene::Update(void * params, Game * game)
{
    game->keyboard.UpdateState();

    if(game->keyboard.IsKeyPressed(SDLK_y))
        character.NextAnimation();

    if(game->keyboard.IsKeyPressed(SDLK_w))
        character.SetCurrentDirection(0);
    
    if(game->keyboard.IsKeyPressed(SDLK_s))
        character.SetCurrentDirection(1);

    if(game->keyboard.IsKeyPressed(SDLK_a))
        character.SetCurrentDirection(2);

    if(game->keyboard.IsKeyPressed(SDLK_d))
        character.SetCurrentDirection(3);
   
    if(game->keyboard.IsKeyDown(SDLK_q))
        game->Kill();

    if(game->keyboard.IsKeyPressed(SDLK_ESCAPE))
    {
        MainMenu * menu = new MainMenu();
        menu->AddMenuItem("Quit");
        menu->AddMenuItem("Continue");
        game->PushScene(menu);
    }

    if(game->keyboard.IsKeyDown(SDLK_RIGHT))
    {
        currentBackground.Move(-1.0f,0.0f);
        character.SetCurrentDirection(3);
        character.StepAnimation();
    }
    if(game->keyboard.IsKeyDown(SDLK_LEFT))
    {
        currentBackground.Move(1.0f,0.0f);
        character.SetCurrentDirection(2);
        character.StepAnimation();
    }
    if(game->keyboard.IsKeyDown(SDLK_UP))
    {
        currentBackground.Move(0.0f,1.0f);
        character.SetCurrentDirection(0);
        character.StepAnimation();
    }
    if(game->keyboard.IsKeyDown(SDLK_DOWN))
    {
        currentBackground.Move(0.0f,-1.0f);
        character.SetCurrentDirection(1);
        character.StepAnimation();
    }
}

void GameScene::Render(void * params, Game * game) const
{
    currentBackground.Render(params, game);
    character.Render(params, game);
    
    list<Renderable*>::const_iterator it;
    for(it = renderables.begin(); it != renderables.end(); it++)
        (*it)->Render(params,game);
}

void GameScene::SceneStackCallback(Scene * scene, Game * game)
{
    int selected = ((MainMenu *)scene)->GetCurrentItem();
    if(selected == 0)
        game->SetScene(new HomeScene());
}
